<?php

namespace console\controllers;

use Yii;
use yii\db\Command;

class ParseController extends \yii\console\Controller 
{

	protected $games = [];
	protected $competitions = [];

	public function actionRun()
	{
		$this->parseXml();

		$this->insertToDatabase();
	}

	protected function Parsexml()
	{
		if (file_exists('/var/www/html/sport_data.xml')) {
		    $xml = simplexml_load_file('/var/www/html/sport_data.xml');
		 	
		} else {
		    exit('Не удалось открыть файл sport_data.xml');
		}
		$j = 1;
		foreach ($xml->Sport->Competitions->Competition as $competition) {
			
			$this->competitions[] = [
				'old_id' => (int)$competition->ID,
				'sport_id' => 1,
				'name' => (string)$competition->Name,
			];

			for ($i = 0; $i < count($competition->Games->ID); $i++) { 
				$this->games[] = [
					'old_id' => (int)$competition->Games->ID[$i],
					'competition_id' => $j,
					'name' => (string)$competition->Games->Name[$i],
					'start' => (string)$competition->Games->Start[$i],
				];
			}
			$j++;
		}
	}

	protected function insertToDatabase()
	{
		Yii::$app->db->createCommand()->batchInsert('competitions', ['old_id', 'sport_id', 'name'], $this->competitions)->execute();
		Yii::$app->db->createCommand()->batchInsert('games', ['old_id', 'competition_id', 'name', 'start'], $this->games)->execute();	
	}

}