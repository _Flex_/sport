<?php

use yii\db\Migration;

class m160418_092022_tbl_competitions extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%competitions}}', [
            'id' => $this->primaryKey(),
            'old_id' => $this->integer(),
            'sport_id' => $this->integer(),
            'name' => $this->string()->notNull()->unique(),
            
        ], $tableOptions);

        $this->createIndex('sport_id', '{{%competitions}}', 'sport_id');

        $this->addForeignKey('fk-competitions-sport_id', '{{%competitions}}', 'sport_id', '{{%sports}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk-competitions-sport_id', '{{%competitions}}');
        $this->dropTable('{{%competitions}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
