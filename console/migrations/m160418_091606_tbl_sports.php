<?php

use yii\db\Migration;

class m160418_091606_tbl_sports extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sports}}', [
            'id' => $this->primaryKey(),
            'old_id' => $this->integer(),
            'name' => $this->string()->notNull()->unique(),
        ], $tableOptions);

        $this->insert('{{%sports}}', [
            'old_id' => 844,
            'name' => 'Football',
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%sports}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
