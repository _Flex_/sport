<?php

use yii\db\Migration;

class m160418_104454_tbl_games extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%games}}', [
            'id' => $this->primaryKey(),
            'old_id' => $this->integer(),
            'competition_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'start' => $this->dateTime(),
            
        ], $tableOptions);

        $this->createIndex('competition_id', '{{%games}}', 'competition_id');

        $this->addForeignKey('fk-games-competition_id', '{{%games}}', 'competition_id', '{{%competitions}}', 'id', 'CASCADE');
    }

    public function down()
    {
        /*$this->dropForeignKey('fk-games-competition_id', '{{%games}}');*/
        $this->dropTable('{{%games}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
