<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "games".
 *
 * @property integer $id
 * @property integer $old_id
 * @property integer $competition_id
 * @property string $name
 * @property string $start
 *
 * @property Competitions $competition
 */
class Games extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'games';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['old_id', 'competition_id'], 'integer'],
            [['name'], 'required'],
            [['start'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['competition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Competitions::className(), 'targetAttribute' => ['competition_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'old_id' => 'Old ID',
            'competition_id' => 'Competition ID',
            'name' => 'Name',
            'start' => 'Start',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetition()
    {
        return $this->hasOne(Competitions::className(), ['id' => 'competition_id']);
    }
}
