<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $competition->name;
$this->params['breadcrumbs'][] = ['label' => 'Footbal', 'url' => ['/site/football', 'id' => 1]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    
	<?php foreach ($games as $game) { ?>

			<div class="list-group">
			  <li class="list-group-item"><span class="badge"><?=$game->start; ?></span> <?=$game->name; ?></li>
			</div>

	<?php	}?>

</div>
