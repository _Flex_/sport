<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Football';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
	
    <p>Association football, more commonly known as football or soccer, is a sport played between two teams of eleven players with a spherical ball. It is played by 250 million players in over 200 nations, making it the world's most popular sport. The game is played on a rectangular field with a goal at each end. The object of the game is to score by getting the ball into the opposing goal.</p>

	<p>The goalkeepers are the only players allowed to touch the ball with their hands or arms while it is in play and only in their penalty area. Outfield players mostly use their feet to strike or pass the ball, but may also use their head or torso to do so instead. The team that scores the most goals by the end of the match wins. If the score is level at the end of the game, either a draw is declared or the game goes into extra time and/or a penalty shootout depending on the format of the competition. The Laws of the Game were originally codified in England by The Football Association in 1863. Association football is governed internationally by the International Federation of Association Football (FIFA; French: Fédération Internationale de Football Association), which organises World Cups for both men and women every four years.</p>

	<div class="row">
	<?php foreach ($competitions as $comp) { ?>

			<div class="col-md-3" style="min-height: 300px;">
				<a href="<?=Url::toRoute(['site/games', 'id' => $comp->id]);?>">
					<?= Html::img('@web/images/'.rand(1, 5).'.jpg', ['alt' => 'picture', 'class' => 'img-responsive']) ?>
					<h3 class="text-center"><?=$comp->name; ?></h3>
				</a>
			</div>

	<?php	}?>
	</div>

	<?php 
		echo LinkPager::widget([
		    'pagination' => $pages,
		]);
	?>

</div>
